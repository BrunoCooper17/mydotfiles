;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' aftermodifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "BrunoCooper17"
user-mail-address "BrunoCooper17@outlook.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

(setq doom-font (font-spec :family "FiraCode Nerd Font Mono" :size 14)
doom-variable-pitch-font (font-spec :family "FiraCode Nerd Font Mono" :size 16))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;(setq doom-theme 'doom-dracula)
(setq doom-theme 'doom-moonlight)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(after! org
(setq org-directory "~/Dropbox/OrgNotes/")
(setq org-agenda-files
'("~/Dropbox/OrgNotes/PersonalNotes/" "~/Dropbox/OrgNotes/GoldenPie/" "~/Dropbox/OrgNotes/Calendars/"))
)

;; If you use `org-jounal' and don't want your journals in the default location below
;; change `org-jounal-directoy'. It must be set before org loads
(setq org-journal-dir "~/Dropbox/OrgNotes/Journal/")
(setq org-journal-file-format "%Y-%m-%d.org")
(setq org-journal-date-format "%A, %d %B %Y")
(setq org-journal-file-type 'monthly)

;; Added a way to open and navigate the journal
(map! :leader
(:prefix-map ("n" . "notes")
(:prefix ("j" . "journal")
:desc "Open current journal" "o" #'org-journal-open-current-journal-file
:desc "Next journal entry" "h" #'org-journal-next-entry
:desc "Previous journal entry" "l" #'org-journal-previous-entry)))

;; Added Google calendar support
(require 'org-gcal)
(setq org-gcal-client-id "800116018783-cgueumdkcd013sujfcndhc1n14prigmq.apps.googleusercontent.com"
org-gcal-client-secret "7m-dOvwP7r6_QkCOAajjKGsu"
org-gcal-fetch-file-alist '(("logico.mastache@gmail.com" . "~/Dropbox/OrgNotes/Calendars/personal.org")
                                ("abygalam@gmail.com" . "~/Dropbox/OrgNotes/Calendars/gaby.org"))
org-gcal-recurring-events-mode 'nested
org-gcal-down-days 30
org-gcal-up-days 30
org-gcal-remove-api-cancelled-events t
org-gcal-remove-cancelled-events t)
;; Added Google calendar keybindings
(map! :leader
(:prefix-map ("o" . "open")
(:prefix("a" . "org agenda")
:desc "Sync Google Calendar" "s" #'org-gcal-sync
:desc "Fetch Google Calendar" "f" #'org-gcal-fetch)))


;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; enable bold and italics in font
(after! doom-themes
:config
(setq doom-themes-enable-bold t
doom-themes-enable-italic t)
;; Corrects (and improves) org-mode's native fontification
(doom-themes-org-config))

; Simpleclip
(simpleclip-mode 1)

; By default, start maximized
(add-to-list 'default-frame-alist '(fullscreen . maximized))

; Set the priorities for each level of task
(setq org-fancy-priorities-list '("⚡" "⬆" "⬇" "☕"))
