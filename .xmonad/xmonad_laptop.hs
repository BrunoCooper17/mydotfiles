--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

import XMonad
import Control.Monad
--import Data.Monoid
import System.Exit

import XMonad.Config.Desktop

-- Actions
import XMonad.Actions.WithAll

-- Hooks
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers

-- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Column
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.WindowNavigation
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
--import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing

-- Utils
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import qualified XMonad.StackSet as W
import qualified Data.Map        as M


-- Order screens by physical location
import XMonad.Actions.PhysicalScreens
import Data.Default
-- For getSortByXineramaPhysicalRule
-- import XMonad.Layout.LayoutCombinators


-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal :: String
myTerminal = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth :: Dimension
myBorderWidth = 3

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask :: KeyMask
myModMask = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces :: [String]
myWorkspaces = ["WWW", "Code", "Terminal", "GFX", "Music", "Chat"]

currentLayout :: X String
currentLayout = gets windowset >>= return . description . W.layout . W.workspace . W.current

-- currentWorkspace :: X String
-- currentWorkspace = gets windowset >>= return . W.currentTag

currentScreen :: X Rational
currentScreen = gets windowset >>= return . toRational . W.screen . W.current

-- Script to move the pointer
scriptMoveCursor :: X()
--scriptMoveCursor = "python3 $HOME/.Scripts/MoveCursor.py"
scriptMoveCursor = currentScreen >>= \cs -> spawn("python3 $HOME/.Scripts/MoveCursor.py " ++ show cs)


-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor :: String
myNormalBorderColor  = "#dddddd"
myFocusedBorderColor :: String
myFocusedBorderColor = "#ff0000"


------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@XConfig {XMonad.modMask = modm} = M.fromList $
    [
    --  Reset the layouts on the current workspace to default
      ((modm .|. shiftMask, xK_space ), setLayout (XMonad.layoutHook conf) >> scriptMoveCursor)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), (windows $ f i) >> scriptMoveCursor)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{q,w,e}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{q,w,e}, Move client to screen 1, 2, or 3
    --
    -- Order screen by physical order instead of arbitrary numberings.
    [((m .|. modm, key), (f sc) >> scriptMoveCursor)
      | (key, sc) <- zip [xK_q, xK_w, xK_e] [0..]
      , (f, m) <- [(viewScreen def, 0), (sendToScreen def, shiftMask)]]


myAltKeys :: [(String, X())]
myAltKeys =
  [
    -- XMonad
    ("M-y",   spawn "xmonad --recompile; xmonad --restart")  -- Recompile and restore XMonad
  , ("M-S-y", io exitSuccess)                                  -- Quit Xmonad :)


  -- Most useful keybinds
  , ("M-p",          spawn "rofi -show combi")   -- The rofi launcher
  , ("M-S-<Return>", spawn myTerminal)           -- My terminal emulator
  , ("M-S-l",        spawn "i3lock-fancy-multimonitor --blur=5x4") -- Screen locker
  , ("<Print>",      spawn "spectacle")          -- Take a screenshot

  , ("M-S-c", kill >> scriptMoveCursor)    -- Kill the focused window
  , ("M-S-a", killAll >> scriptMoveCursor) -- Kill all windows on current workspace
  , ("M-S-<Escape>", spawn ("echo \"" ++ help ++ "\" | xmessage -file - -font -misc-hack-bold-r-normal--14-0-0-0-m-0-iso10646-1 -bg \"#282A2E\" -fg \"#C5C8C6\" -default okay"))


  -- Layout controls
  , ("M-<Space>", sendMessage NextLayout >> scriptMoveCursor) -- Change to next Layout
  , ("M-n", refresh >> scriptMoveCursor) -- Resize current window

  , ("M-<Tab>",    windows W.focusDown >> scriptMoveCursor) -- Move Focus Down
  , ("M-<Down>",   windows W.focusDown >> scriptMoveCursor)
  , ("M-S-<Tab>",  windows W.focusUp >> scriptMoveCursor)   -- Move Focus Up
  , ("M-<Up>",     windows W.focusUp >> scriptMoveCursor)

  , ("M-S-<Down>", windows W.swapDown >> scriptMoveCursor) -- Swap the focused window with the next one
  , ("M-S-<Up>",   windows W.swapUp >> scriptMoveCursor)   -- Swap the focused window with the previous one

  , ("M-S-<Left>",  sendMessage Shrink >> scriptMoveCursor) -- Shrink the master area
  , ("M-S-<Right>", sendMessage Expand >> scriptMoveCursor) -- Expand the master area
  --, ("M-C-<Up>",    sendMessage MirrorShrink >> scriptMoveCursor) -- Only in RTall
  --, ("M-C-<Down>",  sendMessage MirrorExpand >> scriptMoveCursor) -- Only in RTall

  , ("M-S-g", sendMessage (Toggle MIRROR) >> scriptMoveCursor) -- Toggle MIRROR
  , ("M-S-f", sendMessage ToggleStruts >> scriptMoveCursor)    -- Toogle the status bar gap

  , ("M-m",        windows W.focusMaster >> scriptMoveCursor) -- Focus the master window
  , ("M-<Return>", windows W.swapMaster >> scriptMoveCursor)  -- Set the new master window

  , ("M-x", sendMessage (IncMasterN 1) >> scriptMoveCursor)    -- Increase the number of masters
  , ("M-z", sendMessage (IncMasterN (-1)) >> scriptMoveCursor) -- Decrease the number of masters
  , ("M-S-x", increaseLimit) -- Increase number of windows
  , ("M-S-z", decreaseLimit) -- Decrease number of windows


  -- Spawn Most used apps (Maybe in the future use a grid launcher)
  , ("M-o M-e", spawn "emacs") -- Emacs client


  -- Spawn a web browser
  , ("M-b M-1", spawn "vivaldi-stable")
  , ("M-b M-2", spawn "brave")
  , ("M-b M-3", spawn "chromium")
  , ("M-b M-4", spawn "firefox")
  , ("M-b M-5", spawn "firefox --private-window")
  ]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings XConfig {XMonad.modMask = modm} = M.fromList
    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), \w -> focus w >> windows W.shiftMaster)

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- The default number of windows in the master pane
nmasters :: Int
nmasters = 1

-- Default proportion of screen occupied by master pane
ratio :: Rational
ratio = 2/3

-- Percent of screen to increment by when resizing panes
delta :: Rational
delta = 1/100

tall     = mkToggle (single MIRROR)
         $ renamed [Replace "RTall"]
         $ windowNavigation
         $ limitWindows 6
         $ mySpacing 3
         $ ResizableTall nmasters delta ratio []

grid     = mkToggle (single MIRROR)
         $ renamed [Replace "Grid"]
         $ windowNavigation
         $ limitWindows 12
         $ mySpacing 3
         $ Grid (4/3)

column   = mkToggle (single MIRROR)
         $ renamed [Replace "Column"]
         $ windowNavigation
         $ limitWindows 3
         $ mySpacing 3
         $ Column 1.0

full     = renamed [Replace "Full"]
         $ mySpacing 3 Full

--myLayout = avoidStruts $ desktopLayoutModifiers (tall ||| full ||| grid ||| column)

myLayout = avoidStruts $ desktopLayoutModifiers
  $ onWorkspace "WWW"      ( full ||| grid ||| column)
  $ onWorkspace "Code"     ( tall ||| full )
  $ onWorkspace "Terminal" ( tall ||| grid ||| column ||| full )
  $ onWorkspace "GFX"      ( full ||| tall )
  $ onWorkspace "Music"    ( grid ||| tall ||| full )
  $ onWorkspace "Chat"     ( grid ||| tall ||| full )

  tall  -- Default for every workspace without a layout defined


------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = (composeAll . concat $
    [ [ resource  =? r --> doIgnore           | r <- myIgnores   ]
    , [ className =? c --> doShift "WWW"      | c <- myCWWW      ]
    , [ title     =? t --> doShift "WWW"      | t <- myTWWW      ]

    , [ className =? c --> doShift "Code"     | c <- myCCode     ]
    , [ title     =? t --> doShift "Code"     | t <- myTCode     ]

    , [ className =? c --> doShift "Terminal" | c <- myCTerminal ]
    , [ title     =? t --> doShift "Terminal" | t <- myTTerminal ]

    , [ className =? c --> doShift "GFX"      | c <- myCGFX      ]
    , [ title     =? t --> doShift "GFX"      | t <- myTGFX      ]

    , [ className =? c --> doShift "Music"    | c <- myCMusic    ]
    , [ title     =? t --> doShift "Music"    | t <- myTMusic    ]

    , [ className =? c --> doShift "Chat"     | c <- myCChat     ]
    , [ title     =? t --> doShift "Chat"     | t <- myTChat     ]

    , [ className =? c --> doFloat            | c <- myCFloats   ]
    , [ title     =? t --> doFloat            | t <- myTFloats   ]
    , [ name      =? n --> doFloat            | n <- myNames     ]

    , [ isFullscreen   --> myDoFullFloat                         ]
    ])
  where
    role = stringProperty "WM_WINDOW_ROLE"
    name = stringProperty "WM_NAME"

    -- classNames & titles
    myCWWW      = ["Vivaldi-stable", "Brave-browser", "firefox", "Chromium", "Google-chrome"]
    myTWWW      = []
    myCCode     = ["Emacs", "code-oss", "Atom", "jetbrains-clion", "UE4Editor"]
    myTCode     = []
    myCTerminal = [] -- Alacritty
    myTTerminal = []
    myCGFX      = ["Gimp", "krita", "Steam", "Blender", "TexturePacker"]
    myTGFX      = []
    myCMusic    = ["elisa", "Spotify", "Stremio", "Guitarix"]
    myTMusic    = ["Cadence"]
    myCChat     = ["Slack", "discord", "Whatsapp-for-linux"]
    myTChat     = ["FacebookMessenger", "WhatsApp"]

    -- Floats
    myCFloats   = ["Dialog", "Progress"]
    myTFloats   = []

    -- resources
    myIgnores = ["desktop", "desktop_window", "kdesktop", "notify-osd", "trayer"]

    -- names
    myNames = ["Google Chrome Options", "About Mozilla Firefox", "Chromium Options"]

-- a trick for fullscreen but stil allow focusing of other WSs
myDoFullFloat :: ManageHook
myDoFullFloat = doF W.focusDown <+> doFullFloat


------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = ewmhDesktopsEventHook <+> fullscreenEventHook


------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--

myLogHook = do
  fadeInactiveLogHook 0.9
  currentLayout >>= \cl -> io $ appendFile "/tmp/xmonad-layout-log"     (cl ++ "\n")
  currentScreen >>= \cs -> io $ appendFile "/tmp/xmonad-screen-log"     (show cs ++ "\n")
  --currentScreen >>= \cs -> io $ appendFile "/tmp/xmonad-screen-alt-log" (show cs ++ "\n")


------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
  spawnOnce "lxsession &"
  spawnOnce "/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh &"
  spawnOnce "picom &"
  spawnOnce "nitrogen --restore &"
  spawnOnce "/usr/bin/emacs --daemon &"
  spawnOnce "/usr/bin/dunst &"
  spawnOnce "nm-applet &"
  spawnOnce "dropbox &"
  spawnOnce "~/.Scripts/HuionConfig.sh &"
  spawn     "~/.config/polybar/launch.sh &"


-- Run xmonad with the settings you specify. No need to modify this.
--
main :: IO ()
main = do
  let logPipes = [
          "xmonad-layout-log"
        , "xmonad-screen-log"
        , "xmonad-screen-alt-log" ]
  forM_ logPipes $ \file -> do
    safeSpawn "mkfifo" ["/tmp/" ++ file]

  xmonad $ ewmh $ desktopConfig {
    -- simple stuff
      terminal           = myTerminal
    , focusFollowsMouse  = myFocusFollowsMouse
    , clickJustFocuses   = myClickJustFocuses
    , borderWidth        = myBorderWidth
    , modMask            = myModMask
    , workspaces         = myWorkspaces
    , normalBorderColor  = myNormalBorderColor
    , focusedBorderColor = myFocusedBorderColor

     -- key bindings
    , keys               = myKeys
    , mouseBindings      = myMouseBindings

    -- hooks, layouts
    , layoutHook         = myLayout
    , manageHook         = manageDocks <+> myManageHook <+> manageHook desktopConfig
    , handleEventHook    = docksEventHook <+> myEventHook <+> handleEventHook desktopConfig
    , logHook            = myLogHook <+> ewmhDesktopsLogHook <+> logHook desktopConfig
    , startupHook        = myStartupHook
    } `additionalKeysP` myAltKeys


-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines [
    "-------------------------------------------",
    "   Default keybindings (use Emacs style)   ",
    "-------------------------------------------",
    "M -> Win Key.  S -> Shift.",
    "A -> Left Alt. C -> Left Control",
    "",
    "------------",
    "   XMonad   ",
    "------------",
    "M-y    Recompile and restart XMonad",
    "M-S-y  Quit XMonad :)",
    "",
    "-----------------------------",
    "   Most Useful keybindings   ",
    "-----------------------------",
    "M-p           Open rofi",
    "M-S-<Return>  Open a terminal emulator",
    "M-S-l         Lock the screen",
    "<Print>       Take screenshot",
    "",
    "M-S-c         Kill the focused window",
    "M-S-a         Kill all the windows on current workspace",
    "M-S-<Escape>  Show this help",
    "",
    "---------------------",
    "   Layout controls   ",
    "---------------------",
    "M-<space>    Change to next layout",
    "M-S-<Space>  Reset the layouts on the current workSpace to default",
    "M-n          Resize/Refresh current window to the correct size (floating)",
    "",
    "M-<Tab> or M-<Down>  Move Focus Down",
    "M-S-<Tab> or M-<Up>  Move Focus Up",
    "M-S-<Down>           Swap the focused window with the next one",
    "M-S-<Up>             Swap the focused window with the previous one",
    "",
    "M-S-<Left>   Shrink the master area (RTall only)",
    "M-S-<Right>  Shrink the master area (RTall only)",
    "",
    "M-S-g       Toggle MIRROR",
    "M-S-f       Toggle the status bar gap",
    "M-m         Focus the master window",
    "M-<Return>  Set the focused window as master",
    "",
    "M-x    Increase the number of masters",
    "M-z    Decrease the number of masters",
    "M-S-x  Increase the number of windows displayed",
    "M-S-z  Decrease the number of windows displayed",
    "",
    "--------------------------",
    "   Spawn most used Apps   ",
    "--------------------------",
    "M-o M-e       Open and Emacs client",
    "M-b M-[1..5]  Open a web browser",
    "",
    "--------------------------",
    "   Workspaces & screens   ",
    "--------------------------",
    "M-[1..9]     Switch to workSpace N",
    "M-S-[1..9]   Move client to workspace N",
    "M-{w,e,r}    Switch to physical/Xinerama screens 1, 2, or 3",
    "M-S-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-------------------------------------------",
    "   Mouse bindings:                         ",
    "   Default actions bound to mouse events   ",
    "-------------------------------------------",
    "M-button1  Set the window to floating mode and move by dragging",
    "M-button2  Raise the window to the top of the stack",
    "M-button3  Set the window to floating mode and resize by dragging",

    "-- floating layer support",
    "M-t  Push window back into tiling; unfloat and re-tile it"]
