#! /bin/bash
# Setup HUION WH1409, after bridged to wacom driver with Digimend Kernel module.
# License: CC-0/Public-Domain license
# author: deevad

sleep 10

# Tablet definition
tabletstylus="Tablet Monitor Pen stylus"
tabletpad="Tablet Monitor Pad pad"

# Reset
xsetwacom --set "$tabletstylus" ResetArea
xsetwacom --set "$tabletstylus" RawSample 4

# Mapping
# get maximum size geometry with:
#xsetwacom --get "$tabletstylus" Area
# 0 0 55200 34500
# 0 0 53580 33640
tabletX=53580
tabletY=33640
# screen size:
screenX=1920
screenY=1080
# map to good screen (dual nvidia)
xsetwacom --set "$tabletstylus" MapToOutput "HEAD-1"
# setup ratio :
newtabletY=$(( $screenY * $tabletX / $screenX ))
xsetwacom --set "$tabletstylus" Area 0 0 "$tabletX" "$tabletY"
#xsetwacom --set "$tabletstylus" Area 0 0 "$tabletX" "$newtabletY"
